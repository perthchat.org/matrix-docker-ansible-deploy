
# Matrix Repmgr

A solution for clustering the Synapse database across a bunch of standby machines. When a
Matrix server becomes too large the time required for a full dump/restore of the DB makes 
the service immobile and hard to migrate quickly.

## Getting Started

1) After setting up your primary server you'll need to first setup 1 to N standby servers. 
In this example the primary servers (matrix.perthchat.org) database is being replicated to
two standby servers. (pg1.example.org and pg2.example.org)

2) Add the following section to your primary servers vars.yml:
```
# Repmgr Settings
matrix_repmgr_enabled: true
matrix_repmgr_hosts_list: ['matrix.example.org', 'pg1.example.org', 'pg2.example.org']
matrix_repmgr_primary_host: 'matrix.example.org'
matrix_repmgr_connection_password: << new string password >>
```

3) Create a simplified vars.yml for each standby server, a few settings need to be copied 
from your primary servers config:
```
# Basic Settings
matrix_domain: example.org
matrix_synapse_macaroon_secret_key: << same as primary >>
matrix_homeserver_generic_secret_key: << same as primary >>

matrix_synapse_enabled: true
matrix_client_element_enabled: false
matrix_ma1sd_enabled: false
matrix_bot_mjolnir_enabled: false
matrix_jitsi_enabled: false
matrix_coturn_enabled: false
matrix_mailer_enabled: false
matrix_nginx_proxy_enabled: false

# PostgreSQL Settings
matrix_postgres_connection_password: << same as primary >>
matrix_synapse_connection_password: << same as primary >>
matrix_postgres_process_extra_arguments: [
  << same as primary >>
]

matrix_ssl_retrieval_method: none

# Repmgr Settings
matrix_repmgr_enabled: true
matrix_repmgr_hosts_list: ['matrix.example.org', 'pg1.example.org', 'pg2.example.org']
matrix_repmgr_primary_host: 'matrix.example.org'
matrix_repmgr_connection_password: << same as primary >>
```

3) Finally run the playbook again with the setup-all and start tag:
`$ ansible-playbook -i inventory/hosts setup.yml --tags=setup-all,start`

## Print Status

## Migrating With Repmgr